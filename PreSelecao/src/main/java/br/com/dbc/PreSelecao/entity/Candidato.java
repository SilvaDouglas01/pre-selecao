package br.com.dbc.PreSelecao.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author jaqueline.bonoto
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CANDIDATO")
public class Candidato extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @SequenceGenerator(name = "S_CANDIDATO", sequenceName = "S_CANDIDATO", allocationSize = 1)
    @GeneratedValue(generator = "S_CANDIDATO", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "NOME")
    private String nomeCompleto;

    @NotNull
    @Column(name = "DATA_NASCIMENTO")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataNascimento;

    @NotNull
    @Column(name = "DATA_INSCRICAO")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime dataInscricao;

    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "CPF")
    private String cpf;
    
    @NotNull
    @Size(min = 10, max = 64)
    @Column(name = "EMAIL")
    private String email;

    @NotNull
    @Column(name = "STATUS")
    private Status status;
    
    @NotNull
    @Column(name = "ETAPA")
    private Etapa etapa;

    @NotNull
    @Column(name = "CURRICULO")
    private String curriculo;

    @NotNull
    @Size(min = 8, max = 14)
    @Column(name = "TELEFONE")
    private String telefone;
    
    @NotNull
    @Size(min = 8, max = 14)
    @Column(name = "CELULAR")
    private String celular;

    @NotNull
    @JoinColumn(name = "ID_ENDERECO", unique = true)
    @OneToOne(cascade = CascadeType.ALL)
    private Endereco endereco;
    
    @NotNull
    @JoinColumn(name = "ID_EDICAO", referencedColumnName = "ID")
    @ManyToOne
    private Edicao edicao;

    @NotNull
    @JoinColumn(name = "ID_NOTAS", unique = true, referencedColumnName = "ID")
    @OneToOne(cascade = CascadeType.ALL)
    private Notas notas;

    @NotNull
    @JoinColumn(name = "ID_QUESTIONARIO", unique = true, referencedColumnName = "ID")
    @OneToOne(cascade = CascadeType.ALL)
    private Questionario questionario;  
    
    @Column(name = "COMENTARIO")
    private String comentario;
}
