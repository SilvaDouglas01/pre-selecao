package br.com.dbc.PreSelecao.entity;

/**
 *
 * @author henrique.kalife
 */
public enum Etapa {
    PRE_SELECAO, PROCESSO_SELETIVO, SELECIONADO;
}
