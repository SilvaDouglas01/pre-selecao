package br.com.dbc.PreSelecao.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author jonas.cruz
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CandidatoDTO {

    private String nomeCompleto;

    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate nascimento;

    private String cpf;

    private String telefone;
    
    private String celular;

    private String cep;

    private String logradouro;

    private Long numero;

    private String complemento;

    private String bairro;

    private String cidade;

    private String estado;

    private String email;

    private String instituicao;

    private String curriculo;
    
    private QuestionarioDTO questionario;
}
