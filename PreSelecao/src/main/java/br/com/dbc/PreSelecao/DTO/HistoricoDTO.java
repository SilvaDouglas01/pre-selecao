package br.com.dbc.PreSelecao.DTO;

import br.com.dbc.PreSelecao.DTO.MediaDTO;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HistoricoDTO {

    List<MediaDTO> mediasEdicao;
}
