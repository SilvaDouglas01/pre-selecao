package br.com.dbc.PreSelecao.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.silva
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Questionario extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @SequenceGenerator(name = "S_QUESTIONARIO", sequenceName = "S_QUESTIONARIO", allocationSize = 1)
    @GeneratedValue(generator = "S_QUESTIONARIO", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "MATRICULA")
    private boolean matriculadoEmCurso;

    @NotNull
    @Column(name = "CURSO")
    private String curso;
    
    @NotNull
    @Column(name = "INSTITUICAO")
    private String instituicao;

    @NotNull
    @Column(name = "TURNO")
    private String turnos;

    @NotNull
    @Column(name = "MOTIVOS")
    private String motivo;

    @NotNull
    @Column(name = "CONHECIMENTO_EM_LOGICA")
    private boolean conhecimentoEmLogica;

    @NotNull
    @Column(name = "DISPONIBILIDADE_DE_HORARIO")
    private boolean disponibilidadeDeHorario;

    @NotNull
    @Column(name = "DISPONIBILIDADE_APOS_ESTAGIO")
    private boolean disponibilidadeAposEstagio;

    @NotNull
    @Column(name = "INSPIRACAO")
    private String inspiracao;

    @NotNull
    @Column(name = "REFERENCIAS")
    private String referencias;    
}
