package br.com.dbc.PreSelecao.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jaqueline.bonoto
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ENDERECO")
public class Endereco extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @SequenceGenerator(name = "S_ENDERECO", sequenceName = "S_ENDERECO", allocationSize = 1)
    @GeneratedValue(generator = "S_ENDERECO", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "LOGRADOURO")
    private String logradouro;

    @NotNull
    @Column(name = "NUMERO")
    private Long numero;

    @Size(max = 100)
    @Column(name = "COMPLEMENTO")
    private String complemento;

    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "CEP")
    private String cep;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BAIRRO")
    private String bairro;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CIDADE")
    private String cidade;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ESTADO")
    private String estado;
}
