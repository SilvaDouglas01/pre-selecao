package br.com.dbc.PreSelecao.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author henrique.kalife
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NotasDTO {

    private Double preSelecao;

    private Double tig;

    private Double ac;

    private Double logicaProgramacao;

    private Double entrevistaFacilitador;

    private Double entrevistaRh;

    private double notaFinal;
}
