package br.com.dbc.PreSelecao.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "EDICAO")
public class Edicao extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @SequenceGenerator(name = "S_EDICAO", sequenceName = "S_EDICAO", allocationSize = 1)
    @GeneratedValue(generator = "S_EDICAO", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "ANO")
    @Range(min=2018, max=2030)
    private int ano;

    @NotNull
    @Column(name = "NUMERO_EDICAO")
    @Range(min=1, max=4)
    private int numeroEdicao;

    @NotNull
    @Column(name = "DATA_INICIO")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataInicio;
    
    @NotNull
    @Column(name = "DATA_FIM")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataFim;
    
    @NotNull
    @Column(name = "DATA_INICIO_INSCRICAO")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataInicioInscricao;
    
    @NotNull
    @Column(name = "DATA_FIM_INSCRICAO")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataFimInscricao;
    
    @NotNull
    @Column(name = "DATA_INICIO_PROCESSO_SELETIVO")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataInicioProcessoSeletivo;
    
    @NotNull
    @Column(name = "DATA_FIM_PROCESSO_SELETIVO")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataFimProcessoSeletivo;
    
}
