package br.com.dbc.PreSelecao.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.marques
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ComentarioDTO {
    
    private String comentario;
}
