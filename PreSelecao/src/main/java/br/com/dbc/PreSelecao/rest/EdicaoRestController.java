package br.com.dbc.PreSelecao.rest;

import br.com.dbc.PreSelecao.entity.Edicao;
import br.com.dbc.PreSelecao.service.EdicaoService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping("/api/edicao")
public class EdicaoRestController extends AbstractRestController<Edicao, EdicaoService> {

    @Autowired
    private EdicaoService edicaoService;

    @Override
    protected EdicaoService getService() {
        return edicaoService;
    }

    @CrossOrigin
    @PostMapping("/salva") //cadastrar edicao
    public ResponseEntity<?> salvaEdicao(@RequestBody Edicao edicao) throws Exception {
        return ResponseEntity.ok(edicaoService.saveEdicao(edicao));
    }

    @CrossOrigin
    @PutMapping("/editar-edicao/{id}") //atualizar edicao
    public ResponseEntity<?> editarEdicao(@PathVariable Long id, @RequestBody Edicao edicao) throws Exception {
        return ResponseEntity.ok(edicaoService.editEdicao(edicao));
    }

    @CrossOrigin
    @GetMapping("/inscricoes")
    public ResponseEntity<?> getEstadoInscricoes() throws Exception {
        return ResponseEntity.ok(edicaoService.getEstadoInscricoes());
    }
    
    @CrossOrigin
    @GetMapping("/cronograma")
    public ResponseEntity<?> getCronograma() throws Exception{
        return ResponseEntity.ok(edicaoService.getCronograma());
    }
    
    @CrossOrigin
    @GetMapping("/atual")
    public ResponseEntity<?> getEdicaoAtual() throws Exception{
        return ResponseEntity.ok(edicaoService.getEdicaoAtual(LocalDate.now()));
    }
}
