package br.com.dbc.PreSelecao.rest;

import br.com.dbc.PreSelecao.entity.Questionario;
import br.com.dbc.PreSelecao.service.QuestionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author douglas.silva
 */
@RestController
@RequestMapping("/api/questionario")
public class QuestionarioRestController extends AbstractRestController<Questionario, QuestionarioService> {

    @Autowired
    private QuestionarioService questionarioService;

    @CrossOrigin
    @Override
    protected QuestionarioService getService() {
        return questionarioService;
    }
}
