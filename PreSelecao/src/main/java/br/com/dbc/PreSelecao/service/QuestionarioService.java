package br.com.dbc.PreSelecao.service;

import br.com.dbc.PreSelecao.entity.Questionario;
import br.com.dbc.PreSelecao.repository.QuestionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.silva
 */
@Service
@Transactional(readOnly = true)
public class QuestionarioService extends AbstractCrudService<Questionario> {

    @Autowired
    public QuestionarioRepository questionarioRepository;

    @Override
    protected QuestionarioRepository getRepository() {
        return questionarioRepository;
    }
}
