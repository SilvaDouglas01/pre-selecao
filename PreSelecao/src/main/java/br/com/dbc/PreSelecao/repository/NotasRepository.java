package br.com.dbc.PreSelecao.repository;

import br.com.dbc.PreSelecao.entity.Notas;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.marques
 */
public interface NotasRepository extends JpaRepository<Notas, Long>{
    
}
