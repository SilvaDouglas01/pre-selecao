/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.PreSelecao.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.silva
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InscricoesDTO {
    
    public boolean aberta;
    
}
