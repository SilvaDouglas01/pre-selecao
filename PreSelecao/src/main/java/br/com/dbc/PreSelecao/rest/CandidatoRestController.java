package br.com.dbc.PreSelecao.rest;

import br.com.dbc.PreSelecao.DTO.CandidatoDTO;
import br.com.dbc.PreSelecao.DTO.CandidatoFeedBackDTO;
import br.com.dbc.PreSelecao.DTO.ComentarioDTO;
import br.com.dbc.PreSelecao.entity.Candidato;
import br.com.dbc.PreSelecao.entity.Etapa;
import br.com.dbc.PreSelecao.service.CandidatoService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping("/api/candidato")
public class CandidatoRestController extends AbstractRestController<Candidato, CandidatoService> {

    @Autowired
    private CandidatoService candidatoService;

    @Override
    protected CandidatoService getService() {
        return candidatoService;
    }
    
    @CrossOrigin
    @PostMapping("/dto") //cadastrar candidato
    public ResponseEntity<?> salvaCandidato(@RequestBody CandidatoDTO dto) throws Exception {
        return ResponseEntity.ok(candidatoService.salvaCandidatoComEndereco(dto));
    }

    @CrossOrigin
    @PatchMapping("/{id}") //aprovacao ou rejeicao de candidato
    public ResponseEntity<?> feedBackCandidato(@RequestBody CandidatoFeedBackDTO dto) throws Exception {
        return ResponseEntity.ok(candidatoService.feedBackCandidato(dto));
    }

    @CrossOrigin
    @GetMapping("/cpf/{cpf}") //front busca no banco se o cpf já está cadastrado
    public ResponseEntity<?> verificaCPFExistente(@PathVariable String cpf) {
        Candidato candidato = candidatoService.findByCpf(cpf);
        if (candidato == null) {
            return ResponseEntity.ok(Candidato.builder().build());
        } else {
            return ResponseEntity.ok(candidato);
        }
    }

    @CrossOrigin
    @GetMapping("/historico/{cpf}") //front busca no banco o historico de um candidato
    public ResponseEntity<?> verificaHistorico(@PathVariable String cpf) throws Exception {
        return ResponseEntity.ok(candidatoService.verificarHistorico(cpf));
    }

    @CrossOrigin
    @GetMapping("/email/{email}") //front busca no banco se email já é cadastrado
    public ResponseEntity<?> verificaEmailExistente(@PathVariable String email) {
        return ResponseEntity.ok(candidatoService.findByEmail(email));
    }

    @CrossOrigin
    @GetMapping("/acompanhar/{cpf}")
    public ResponseEntity<?> acompanharInscricao(@PathVariable String cpf) {
        return ResponseEntity.ok(candidatoService.acompanharInscricao(cpf));
    }

    @CrossOrigin
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<?> findDateBetween(
            Pageable pageable,
            @RequestParam(value = "start", required = true)
            @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate start,
            @RequestParam(value = "end", required = true)
            @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate end) {
        return ResponseEntity.ok(
                candidatoService.findByDataInscricaoBetween(
                        pageable,
                        start.atTime(0, 0),
                        end.atTime(23, 59, 59)
                )
        );
    }

    @CrossOrigin
    @GetMapping("/edicao/etapa") //listar candidatos de uma determinada edição
    public ResponseEntity<?> findByEdicaoAndEtapa(Pageable pageable,
            @RequestParam(name = "id", required = true) Long id,
            @RequestParam(name = "etapa", required = true) Etapa etapa,
            @RequestParam(name = "nomeCompleto", required = false) String nomeCompleto) {
        return ResponseEntity.ok(candidatoService.
                findByEdicaoIdAndEtapaAndNomeCompletoContainingIgnoreCase(pageable, id, etapa, nomeCompleto));
    }

    @CrossOrigin
    @GetMapping("/edicao") //listar candidatos de uma determinada edição
    public ResponseEntity<?> findByEdicao(Pageable pageable,
            @RequestParam(name = "id", required = true) Long id,
            @RequestParam(name = "nomeCompleto", required = false) String nomeCompleto) {
        return ResponseEntity.ok(candidatoService.
                findByEdicaoIdAndNomeCompletoContainingIgnoreCase(pageable, id, nomeCompleto));
    }

    @CrossOrigin
    @PatchMapping("/comentar/{id}")
    public ResponseEntity<?> adicionarComentario(@PathVariable Long id, @RequestBody ComentarioDTO comentario) {
        return ResponseEntity.ok(candidatoService.addComentario(id, comentario));
    }
}
