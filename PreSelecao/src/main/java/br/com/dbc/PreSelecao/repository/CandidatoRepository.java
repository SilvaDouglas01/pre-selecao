package br.com.dbc.PreSelecao.repository;

import br.com.dbc.PreSelecao.entity.Candidato;
import br.com.dbc.PreSelecao.entity.Etapa;
import br.com.dbc.PreSelecao.entity.Notas;
import br.com.dbc.PreSelecao.entity.Status;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jonas.cruz
 */
public interface CandidatoRepository extends JpaRepository<Candidato, Long> {

    public List<Candidato> findByCpf(String cpf);
    
    public Optional<Candidato> findByEmail(String email);
    
    public List<Candidato> findByEdicaoId(Long id);
    
    public Optional<Candidato> findByEmailAndEdicaoId(String email, Long id);
    
    public Optional<Candidato> findByCpfAndEdicaoId(String cpf, Long id);
    
    public Page<Candidato> findByEdicaoIdAndNomeCompletoContainingIgnoreCase(Pageable pageable, Long id, String nomeCompleto);
    
    public Page<Candidato> findByEtapaAndNomeCompletoContainingIgnoreCase(Pageable pageable, Etapa etapa, String nomeCompleto);
    
    public Page<Candidato> findByEdicaoIdAndEtapaAndNomeCompletoContainingIgnoreCase(Pageable pageable, Long id, Etapa etapa, String nomeCompleto);

    public Page<Candidato> findByDataInscricaoBetween(Pageable pageable, LocalDateTime ini, LocalDateTime fim);
}
