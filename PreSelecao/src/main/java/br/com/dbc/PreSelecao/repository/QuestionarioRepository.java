package br.com.dbc.PreSelecao.repository;

import br.com.dbc.PreSelecao.entity.Questionario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.silva
 */
public interface QuestionarioRepository extends JpaRepository<Questionario, Long> {

}
