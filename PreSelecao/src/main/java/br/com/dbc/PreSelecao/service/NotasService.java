package br.com.dbc.PreSelecao.service;

import br.com.dbc.PreSelecao.DTO.NotasDTO;
import br.com.dbc.PreSelecao.entity.Notas;
import br.com.dbc.PreSelecao.repository.NotasRepository;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.marques
 */
@Service
@Transactional(readOnly = true)
public class NotasService extends AbstractCrudService<Notas> {

    @Autowired
    private NotasRepository notasRepository;

    @Override
    protected NotasRepository getRepository() {
        return notasRepository;
    }

    @Override
    public Notas save(@NotNull @Valid Notas notas){
        notas.setNotaFinal(this.calculaNotaFinal(notas));
        return getRepository().save(notas);
    }

    private double calculaNotaFinal(Notas notas) {
        double notaFinal = 0;
        double notaPsicologica = 0;
        notaPsicologica = ((notas.getTig() * 6) + (notas.getAc() * 4)) / 10;
        notaFinal = ((notaPsicologica * 1) + (notas.getLogicaProgramacao() * 3)
                + (notas.getEntrevistaRh() * 3) + (notas.getEntrevistaFacilitador() * 3)) / 10;
        return notaFinal;
    }
    
    public Notas buildaNotas(NotasDTO notas) {
        return Notas.builder()
                .preSelecao(notas.getPreSelecao())
                .ac(notas.getAc())
                .tig(notas.getTig())
                .logicaProgramacao(notas.getLogicaProgramacao())
                .entrevistaRh(notas.getEntrevistaRh())
                .entrevistaFacilitador(notas.getEntrevistaFacilitador())
                .notaFinal(notas.getNotaFinal())
                .build();
    }
}
