package br.com.dbc.PreSelecao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "NOTAS")
public class Notas {

    @Id
    @NotNull
    @SequenceGenerator(name = "S_NOTAS", sequenceName = "S_NOTAS", allocationSize = 1)
    @GeneratedValue(generator = "S_NOTAS", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "PRE_SELECAO")
    @Range(min=0, max=10)
    private Double preSelecao;

    @NotNull
    @Column(name = "TIG")
    @Range(min=0, max=10)
    private Double tig;

    @NotNull
    @Column(name = "AC")
    @Range(min=0, max=10)
    private Double ac;

    @NotNull
    @Column(name = "LOGICA_PROGRAMACAO")
    @Range(min=0, max=10)
    private Double logicaProgramacao;

    @NotNull
    @Column(name = "ENTREVISTA_FACILITADOR")
    @Range(min=0, max=10)
    private Double entrevistaFacilitador;

    @NotNull
    @Column(name = "ENTREVISTA_RH")
    @Range(min=0, max=10)
    private Double entrevistaRh;
    
    @NotNull
    @Column(name = "NOTA_FINAL")
    @Range(min=0, max=10)
    private double notaFinal;    
}
