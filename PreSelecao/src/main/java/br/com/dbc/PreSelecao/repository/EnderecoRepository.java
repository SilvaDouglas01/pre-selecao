package br.com.dbc.PreSelecao.repository;

import br.com.dbc.PreSelecao.entity.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jonas.cruz
 */
public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

}
