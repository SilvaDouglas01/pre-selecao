package br.com.dbc.PreSelecao.service;

import br.com.dbc.PreSelecao.DTO.CronogramaDTO;
import br.com.dbc.PreSelecao.DTO.InscricoesDTO;
import br.com.dbc.PreSelecao.entity.Edicao;
import br.com.dbc.PreSelecao.repository.CandidatoRepository;
import br.com.dbc.PreSelecao.repository.EdicaoRepository;
import java.time.LocalDate;
import java.util.Optional;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas.silva
 */
@Service
@Transactional(readOnly = true)
public class EdicaoService extends AbstractCrudService<Edicao> {

    @Autowired
    public EdicaoRepository edicaoRepository;

    @Autowired
    public CandidatoRepository candidatoRepository;

    @Autowired
    public EdicaoService edicaoService;

    @Override
    protected EdicaoRepository getRepository() {
        return edicaoRepository;
    }

    public Edicao getEdicaoAtual(LocalDate dataAtual) {
        Optional <Edicao> edicao = edicaoRepository.findFirstByDataFimAfterOrderByDataFimAsc(dataAtual);
        if(edicao.isPresent())
            return edicao.get();
        
        return null;
    }
    
    public Edicao getUltimaEdicao(){
        return edicaoRepository.findFirstByOrderByDataFimDesc();
    }
    
    public CronogramaDTO getCronograma(){
        Edicao edicao = getUltimaEdicao();
        CronogramaDTO cronograma = CronogramaDTO.builder()
                .inscricoes(false)
                .processoSeletivo(false)
                .inicioCapacitacao(false)
                .fimCapacitacao(false)
                .build();
        
        if(edicao.getDataInicioInscricao().isBefore(LocalDate.now().plusDays(1l)) && 
                edicao.getDataFimInscricao().isAfter(LocalDate.now().minusDays(1l))){
            cronograma.setInscricoes(true);
            return cronograma;
        }
        if(edicao.getDataInicioProcessoSeletivo().isBefore(LocalDate.now().plusDays(1l)) && 
                edicao.getDataFimProcessoSeletivo().isAfter(LocalDate.now().minusDays(1l))){
            cronograma.setProcessoSeletivo(true);
            return cronograma;
        }
        if(edicao.getDataInicio().isAfter(LocalDate.now().minusDays(1l)) &&
                edicao.getDataInicioInscricao().isBefore(LocalDate.now().plusDays(1l))){
            cronograma.setInicioCapacitacao(true);
            return cronograma;
        }
        if(edicao.getDataFim().isAfter(LocalDate.now().minusDays(1l)) &&
                edicao.getDataInicioInscricao().isBefore(LocalDate.now().plusDays(1l))){
            cronograma.setFimCapacitacao(true);
            return cronograma;
        }
        return cronograma;
    }
    
    public InscricoesDTO getEstadoInscricoes() throws Exception {
        Edicao edicao = getEdicaoAtual(LocalDate.now());
        if (edicao == null) {
            return InscricoesDTO.builder().build();
        }
        InscricoesDTO inscricoes = InscricoesDTO.builder().build();
        if (LocalDate.now().isAfter(edicao.getDataInicioInscricao().minusDays(1l))
                && LocalDate.now().isBefore(edicao.getDataFimInscricao().plusDays(1l))) {
            inscricoes.setAberta(true);
        } else {
            inscricoes.setAberta(false);
        }
        return inscricoes;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Edicao saveEdicao(@NotNull Edicao entidadeEdicao) throws Exception {
        if (this.checaDatas(entidadeEdicao) == false) {
            throw new Exception();
        }
        Edicao retorno = null;
        if (edicaoRepository.findByAnoAndNumeroEdicao(entidadeEdicao.getAno(), entidadeEdicao.getNumeroEdicao()) == null) {
            if (this.getEdicaoAtual(LocalDate.now()) == null || entidadeEdicao.getDataInicio().isAfter(this.getEdicaoAtual(LocalDate.now()).getDataFim())) {
                retorno = getRepository().save(entidadeEdicao);
            } else {
                throw new Exception();
            }

        } else {
            throw new Exception();
        }
        return retorno;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Edicao editEdicao(@NotNull Edicao entidadeEdicao) throws Exception {
        if (this.checaDatas(entidadeEdicao) == false) {
            throw new Exception();
        }
        return getRepository().save(entidadeEdicao);
    }

    private boolean checaDatas(Edicao edicao) {
        if (edicao.getDataInicioProcessoSeletivo().isBefore(edicao.getDataFimInscricao())
                || edicao.getDataInicio().isBefore(edicao.getDataFimProcessoSeletivo())
                || edicao.getDataInicioInscricao().isAfter(edicao.getDataFimInscricao())
                || edicao.getDataInicioProcessoSeletivo().isAfter(edicao.getDataFimProcessoSeletivo())
                || edicao.getDataInicio().isAfter(edicao.getDataFim())) {
            return false;
        }
        return true;
    }

    @Override
    public void delete(@NotNull Long edicaoId) {
        candidatoRepository.deleteAll(candidatoRepository.findByEdicaoId(edicaoId));
        edicaoRepository.deleteById(edicaoId);
    }
}
