package br.com.dbc.PreSelecao.rest;

import br.com.dbc.PreSelecao.entity.Notas;
import br.com.dbc.PreSelecao.service.NotasService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author douglas.marques
 */
@RestController
@RequestMapping("/api/notas")
public class NotasRestController extends AbstractRestController<Notas, NotasService> {

    @Autowired
    private NotasService notasService;

    @Override
    protected NotasService getService() {
        return notasService;
    }

    @CrossOrigin
    @Override
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody Notas notas) {
        if (!Objects.equals(notas.getId(), id)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(getService().save(notas));
    }
}
