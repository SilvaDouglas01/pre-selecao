package br.com.dbc.PreSelecao.entity;

/**
 *
 * @author jonas.cruz
 */
public enum Status {

    PENDENTE, CONVITE_ENVIADO, PRESENCA_CONFIRMADA, REJEITADA
}
