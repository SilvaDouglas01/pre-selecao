package br.com.dbc.PreSelecao.service;

import br.com.dbc.PreSelecao.DTO.HistoricoDTO;
import br.com.dbc.PreSelecao.DTO.AcompanharInscricaoDTO;
import br.com.dbc.PreSelecao.DTO.CandidatoDTO;
import br.com.dbc.PreSelecao.DTO.CandidatoFeedBackDTO;
import br.com.dbc.PreSelecao.DTO.ComentarioDTO;
import br.com.dbc.PreSelecao.DTO.MediaDTO;
import br.com.dbc.PreSelecao.DTO.QuestionarioDTO;
import br.com.dbc.PreSelecao.entity.Candidato;
import br.com.dbc.PreSelecao.entity.Edicao;
import br.com.dbc.PreSelecao.entity.Endereco;
import br.com.dbc.PreSelecao.entity.Etapa;
import br.com.dbc.PreSelecao.entity.Notas;
import br.com.dbc.PreSelecao.entity.Questionario;
import br.com.dbc.PreSelecao.entity.Status;
import br.com.dbc.PreSelecao.repository.CandidatoRepository;
import br.com.dbc.PreSelecao.repository.QuestionarioRepository;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class CandidatoService extends AbstractCrudService<Candidato> {

    @Autowired
    private CandidatoRepository candidatoRepository;

    @Autowired
    private QuestionarioRepository questionarioRepository;

    @Autowired
    private EnderecoService enderecoService;

    @Autowired
    private EdicaoService edicaoService;

    @Autowired
    private FileService fileService;

    @Autowired
    private MailService mailService;

    @Override
    protected CandidatoRepository getRepository() {
        return candidatoRepository;
    }

    //método chamado pelo rest para salvar o candidato
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Candidato salvaCandidatoComEndereco(CandidatoDTO dto) throws Exception {
        Candidato candidato = convertToCandidato(dto);
        Endereco endereco = convertToEndereco(dto);
        /*User user = convertToUser(dto);*/
        Notas notas = Notas.builder()
                .preSelecao(0.0d)
                .ac(0.0d)
                .tig(0.0d)
                .logicaProgramacao(0.0d)
                .entrevistaRh(0.0d)
                .entrevistaFacilitador(0.0d)
                .notaFinal(0)
                .build();

        if (this.findByCpf(dto.getCpf()) == null) {
            System.out.println("Não existe candidato cadastrado (deu nulo). Irá cadastrar");
            try {
                endereco = enderecoService.save(endereco);
                candidato.setEmail(dto.getEmail());
                candidato.setEndereco(endereco);
                candidato.setEdicao(edicaoService.getEdicaoAtual(LocalDate.now()));
                candidato.setCurriculo(
                        fileService.uploadFile(dto.getCurriculo(), dto.getNomeCompleto())
                );
                candidato.setNotas(notas);
                candidato.setEtapa(Etapa.PRE_SELECAO);
                candidato = salvaCandidato(candidato);
            } catch (IOException e) {
                System.out.println(e);
                throw new Exception("Erro ao tentar salvar o arquivo de curriculo!");
            } catch (Exception e) {
                System.out.println(e);
                throw new Exception(e);
            }
        } else {
            System.out.println("Existe candidato cadastrado (Diferente de nulo). Não cadastrar");
            throw new Exception();
        }
        return candidato;
    }

    //método auxiliar para verificar se candidato email está validade em usuário
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Candidato salvaCandidato(Candidato candidato) throws Exception {
        if (this.findByCpf(candidato.getCpf()) != null) {
            throw new Exception("Erro ao salvar o candidato!");
        }
        candidato = getRepository().save(candidato);
        return candidato;
    }

    //método para transformar o candidato recebido na tela em um candidato no banco
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Candidato convertToCandidato(CandidatoDTO dto) throws Exception {
        Candidato candidato = Candidato.builder()
                .nomeCompleto(dto.getNomeCompleto())
                .dataInscricao(LocalDateTime.now())
                .dataNascimento(dto.getNascimento())
                .cpf(dto.getCpf())
                .telefone(dto.getTelefone())
                .celular(dto.getCelular())
                .status(Status.PENDENTE)
                .questionario(convertToQuestionario(dto.getQuestionario()))
                .build();
        return candidato;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Questionario convertToQuestionario(QuestionarioDTO questionario) {
        Questionario q = Questionario.builder()
                .conhecimentoEmLogica(questionario.isConhecimentoEmLogica())
                .curso(questionario.getCurso())
                .disponibilidadeAposEstagio(questionario.isDisponibilidadeAposEstagio())
                .disponibilidadeDeHorario(questionario.isDisponibilidadeDeHorario())
                .inspiracao(questionario.getInspiracao())
                .instituicao(questionario.getInstituicao())
                .matriculadoEmCurso(questionario.isMatriculadoEmCurso())
                .motivo(questionario.getMotivo())
                .referencias(questionario.getReferencias())
                .turnos(questionario.getTurnos())
                .build();

        return questionarioRepository.save(q);
    }

    //método auxiliar para criar endereco através de candidato informado no front end
    //os dados de endereço informados no front end são preenchidos pelo service dos correios
    public Endereco convertToEndereco(CandidatoDTO dto) {
        Endereco endereco = Endereco.builder()
                .logradouro(dto.getLogradouro())
                .numero(dto.getNumero())
                .complemento(dto.getComplemento())
                .cep(dto.getCep())
                .bairro(dto.getBairro())
                .cidade(dto.getCidade())
                .estado(dto.getEstado())
                .build();
        return endereco;
    }

    //método a ser acionado ao aprovar ou rejeitar um candidato
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Candidato feedBackCandidato(CandidatoFeedBackDTO dto) throws Exception {
        Candidato candidato = getRepository().findById(dto.getId()).get();

        try {
            if (dto.getStatus() == Status.PENDENTE);
            if (dto.getStatus() == Status.REJEITADA) {
                String message = ("<!doctype html>\n"
                        + "<html>\n"
                        + "<head>\n"
                        + "	<title>Vem Ser DBC</title>\n"
                        + "</head>\n"
                        + "<body>\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Ol&aacute;,</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Agradecemos imensamente o teu interesse e inscri&ccedil;&atilde;o no Programa Vem Ser DBC.</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Foram mais de 250 inscri&ccedil;&otilde;es e 90 avan&ccedil;aram para a etapa classificat&oacute;ria que definir&aacute; os participantes que ocupar&atilde;o as 14 vagas.</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Neste momento n&atilde;o foste selecionado(a) para participar da sele&ccedil;&atilde;o do programa.</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Por favor, n&atilde;o deixe de se inscrever nas pr&oacute;ximas edi&ccedil;&otilde;es para participar. Fique ligado nas nossas redes sociais!</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><strong><span style=\"color:#C94062;\">Estar pr&oacute;ximo</span><span style=\"color:#3F56A1;\"> &eacute; crescer juntos.</span></strong></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Atenciosamente,</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\">&nbsp;</p>\n"
                        + "\n"
                        + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"m_-8724565486041811384MsoNormalTable\" style=\"color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: rgb(255, 255, 255); border-collapse: collapse;\">\n"
                        + "	<tbody>\n"
                        + "		<tr style=\"height: 52.1pt;\">\n"
                        + "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 118.1pt; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid rgb(11, 93, 169); padding: 0cm 5.4pt; height: 52.1pt;\" width=\"157\">\n"
                        + "			<p align=\"center\" class=\"MsoNormal\" style=\"margin: 0px; text-align: center;\"><a data-saferedirecturl=\"https://www.google.com/url?q=https://www.facebook.com/VemSerDBC&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNEUA-BWbzqrgStYvzbXtvBYSuA3eA\" href=\"https://www.facebook.com/VemSerDBC\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; color: rgb(31, 73, 125); text-decoration-line: none;\"><img alt=\"cid:image001.jpg@01D42F43.8C41E4F0\" border=\"0\" class=\"CToWUd\" data-image-whitelisted=\"\" height=\"91\" id=\"m_-8724565486041811384Imagem_x0020_18\" src=\"https://uploaddeimagens.com.br/images/001/784/375/original/logoVemSer.jpg?1545047977\" width=\"143\" /></span></a></p>\n"
                        + "			</td>\n"
                        + "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 129pt; padding: 0cm 5.4pt; height: 52.1pt;\" valign=\"top\" width=\"172\">\n"
                        + "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">&nbsp;</span></b></p>\n"
                        + "\n"
                        + "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">Equipe Vem Ser DBC</span></b></p>\n"
                        + "\n"
                        + "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><u><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\"><a data-saferedirecturl=\"https://www.google.com/url?q=http://www.dbccompany.com.br/&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNFW46eVp1pc4aDt8VF1sA3Zmhs9Dw\" href=\"http://www.dbccompany.com.br/\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"color: rgb(63, 86, 161);\">dbc</span><span style=\"color: rgb(201, 64, 98);\">company</span><span style=\"color: rgb(63, 86, 161);\">.com.br</span></a></span></u></b></p>\n"
                        + "			</td>\n"
                        + "		</tr>\n"
                        + "	</tbody>\n"
                        + "</table>\n"
                        + "\n"
                        + "<div>&nbsp;</div>\n"
                        + "</body>\n"
                        + "</html>");
                mailService.sendMail(
                        "emailparatestarfuncionalidades@gmail.com",
                        candidato.getEmail(),
                        "Resultado do Processo Seletivo do Programa Vem Ser",
                        message);
                if (candidato.getEtapa() == Etapa.SELECIONADO) {
                    candidato.setEtapa(Etapa.PROCESSO_SELETIVO);
                    candidato.setStatus(Status.CONVITE_ENVIADO);
                } else {
                    candidato.setStatus(dto.getStatus());
                }
                candidato = getRepository().save(candidato);
            };
            if (dto.getStatus() == Status.CONVITE_ENVIADO) {
                atualizarEtapa(candidato);
                String message = ("<!doctype html>\n"
                        + "<html>\n"
                        + "<head>\n"
                        + "	<title>Vem Ser DBC</title>\n"
                        + "</head>\n"
                        + "<body>\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Ol&aacute;,</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Agradecemos imensamente o teu interesse e inscri&ccedil;&atilde;o no Programa Vem Ser DBC.</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Gostar&iacute;amos de informar que voc&ecirc; foi selecionado para a pr&oacute;xima etapa. Parab&eacute;ns!</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Em breve, voc&ecirc; receber&aacute; informa&ccedil;&otilde;es sobre data e hora.</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><strong><span style=\"color:#C94062;\">Estar pr&oacute;ximo</span><span style=\"color:#3F56A1;\"> &eacute; crescer juntos.</span></strong></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Atenciosamente,</span></span></p>\n"
                        + "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\">&nbsp;</p>\n"
                        + "\n"
                        + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"m_-8724565486041811384MsoNormalTable\" style=\"color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: rgb(255, 255, 255); border-collapse: collapse;\">\n"
                        + "	<tbody>\n"
                        + "		<tr style=\"height: 52.1pt;\">\n"
                        + "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 118.1pt; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid rgb(11, 93, 169); padding: 0cm 5.4pt; height: 52.1pt;\" width=\"157\">\n"
                        + "			<p align=\"center\" class=\"MsoNormal\" style=\"margin: 0px; text-align: center;\"><a data-saferedirecturl=\"https://www.google.com/url?q=https://www.facebook.com/VemSerDBC&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNEUA-BWbzqrgStYvzbXtvBYSuA3eA\" href=\"https://www.facebook.com/VemSerDBC\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; color: rgb(31, 73, 125); text-decoration-line: none;\"><img alt=\"cid:image001.jpg@01D42F43.8C41E4F0\" border=\"0\" class=\"CToWUd\" data-image-whitelisted=\"\" height=\"91\" id=\"m_-8724565486041811384Imagem_x0020_18\" src=\"https://uploaddeimagens.com.br/images/001/784/375/original/logoVemSer.jpg?1545047977\" width=\"143\" /></span></a></p>\n"
                        + "			</td>\n"
                        + "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 129pt; padding: 0cm 5.4pt; height: 52.1pt;\" valign=\"top\" width=\"172\">\n"
                        + "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">&nbsp;</span></b></p>\n"
                        + "\n"
                        + "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">Equipe Vem Ser DBC</span></b></p>\n"
                        + "\n"
                        + "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><u><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\"><a data-saferedirecturl=\"https://www.google.com/url?q=http://www.dbccompany.com.br/&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNFW46eVp1pc4aDt8VF1sA3Zmhs9Dw\" href=\"http://www.dbccompany.com.br/\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"color: rgb(63, 86, 161);\">dbc</span><span style=\"color: rgb(201, 64, 98);\">company</span><span style=\"color: rgb(63, 86, 161);\">.com.br</span></a></span></u></b></p>\n"
                        + "			</td>\n"
                        + "		</tr>\n"
                        + "	</tbody>\n"
                        + "</table>\n"
                        + "\n"
                        + "<div>&nbsp;</div>\n"
                        + "</body>\n"
                        + "</html>");

                mailService.sendMail(
                        "emailparatestarfuncionalidades@gmail.com",
                        candidato.getEmail(),
                        "Resultado do Processo Seletivo do Programa Vem Ser",
                        message
                );
                candidato.setStatus(dto.getStatus());
                candidato = getRepository().save(candidato);
            };
            if (dto.getStatus() == Status.PRESENCA_CONFIRMADA) {
                candidato.setStatus(dto.getStatus());
                candidato = getRepository().save(candidato);
            };
        } catch (Exception e) {
            System.out.println(e);
            throw new Exception(e);
        } finally {
            return candidato;
        }
    }

    public void atualizarEtapa(Candidato candidato) {
        if (candidato.getEtapa() == Etapa.PRE_SELECAO) {
            candidato.setEtapa(Etapa.PROCESSO_SELETIVO);
        } else if (candidato.getEtapa() == Etapa.PROCESSO_SELETIVO) {
            candidato.setEtapa(Etapa.SELECIONADO);
        }
    }

    public Candidato findByCpf(String cpf) {
        try {
            Edicao edicao = edicaoService.getEdicaoAtual(LocalDate.now());
            Optional<Candidato> candidato = candidatoRepository.findByCpfAndEdicaoId(cpf, edicao.getId());
            return candidato.isPresent() ? candidato.get() : null;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public HistoricoDTO verificarHistorico(String cpf) throws Exception {
        List<Candidato> candidatoHistorico = getRepository().findByCpf(cpf);
        if (candidatoHistorico == null || candidatoHistorico.isEmpty()) {
            throw new Exception("Erro ao procurar candidato!");
        }
        if (candidatoHistorico.size() == 1) {
            throw new Exception("Não tem histórico!");
        }
        List<MediaDTO> medias = new ArrayList<>();
        candidatoHistorico.forEach(candidato -> {
            if (candidato.getEdicao() != edicaoService.getEdicaoAtual(LocalDate.now())) {
                medias.add(this.buildMedias(candidato));
            }
        });
        return HistoricoDTO.builder().mediasEdicao(medias).build();
    }

    private MediaDTO buildMedias(Candidato candidatoHistorico) {
        return MediaDTO.builder()
                .ano(candidatoHistorico.getEdicao().getAno())
                .numeroEdicao(candidatoHistorico.getEdicao().getNumeroEdicao())
                .notaFinal(candidatoHistorico.getNotas().getNotaFinal())
                .build();
    }

    public Candidato findByEmail(String email) {
        Edicao edicao = edicaoService.getEdicaoAtual(LocalDate.now());
        Optional<Candidato> candidato = candidatoRepository.findByEmailAndEdicaoId(email, edicao.getId());
        return candidato.isPresent() ? candidato.get() : Candidato.builder().build();
    }

    public AcompanharInscricaoDTO acompanharInscricao(String cpf) {
        Candidato candidato = findByCpf(cpf);

        return AcompanharInscricaoDTO.builder()
                .status(candidato.getStatus())
                .etapa(candidato.getEtapa())
                .build();
    }

    public Page<Candidato> findByEdicaoIdAndEtapaAndNomeCompletoContainingIgnoreCase(Pageable pageable, Long id, Etapa etapa, String nomeCompleto) {
        return candidatoRepository.findByEdicaoIdAndEtapaAndNomeCompletoContainingIgnoreCase(pageable, id, etapa, nomeCompleto);
    }

    public Page<Candidato> findByEdicaoIdAndNomeCompletoContainingIgnoreCase(Pageable pageable, Long id, String nomeCompleto) {
        return candidatoRepository.findByEdicaoIdAndNomeCompletoContainingIgnoreCase(pageable, id, nomeCompleto);
    }

    public Page<Candidato> findByEtapaAndNomeCompletoContainingIgnoreCase(Pageable pageable, Etapa etapa, String nomeCompleto) {
        return candidatoRepository.findByEtapaAndNomeCompletoContainingIgnoreCase(pageable, etapa, nomeCompleto);
    }

    public Page<Candidato> findByDataInscricaoBetween(Pageable pageable, LocalDateTime ini, LocalDateTime fim) {
        return candidatoRepository.findByDataInscricaoBetween(pageable, ini, fim);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Candidato addComentario(Long id, ComentarioDTO comentario) {
        Candidato candidato = getRepository().findById(id).get();
        candidato.setComentario(comentario.getComentario());
        return getRepository().save(candidato);
    }
}
