package br.com.dbc.PreSelecao.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas.silva
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionarioDTO {

    private boolean matriculadoEmCurso;

    private String curso;

    private String instituicao;

    private String turnos;

    private String motivo;

    private boolean conhecimentoEmLogica;

    private boolean disponibilidadeDeHorario;

    private boolean disponibilidadeAposEstagio;

    private String inspiracao;

    private String referencias;
}
