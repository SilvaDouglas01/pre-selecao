package br.com.dbc.PreSelecao.repository;

import br.com.dbc.PreSelecao.entity.Edicao;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas.silva
 */
public interface EdicaoRepository extends JpaRepository<Edicao, Long> {

    public Optional<Edicao> findFirstByDataFimAfterOrderByDataFimAsc(LocalDate atual);

    public Edicao findByAnoAndNumeroEdicao(int ano, int numeroEdicao);

    public Edicao findByAno(int ano);

    public Edicao findFirstByOrderByDataFimDesc();
}
