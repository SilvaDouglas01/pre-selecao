package br.com.dbc.PreSelecao.rest;

import br.com.dbc.PreSelecao.PreSelecaoApplicationTests;
import br.com.dbc.PreSelecao.service.CandidatoService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author douglas.silva
 */
public class QuestionarioRestControllerTest extends PreSelecaoApplicationTests {

    @Autowired
    private QuestionarioRestController questionarioRestController;

    @Autowired
    private CandidatoService candidatoService;

    @Override
    protected AbstractRestController getController() {
        return questionarioRestController;
    }
}
