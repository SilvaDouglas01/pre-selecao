package br.com.dbc.PreSelecao.rest;

import br.com.dbc.PreSelecao.DTO.CandidatoDTO;
import br.com.dbc.PreSelecao.DTO.CandidatoFeedBackDTO;
import br.com.dbc.PreSelecao.DTO.QuestionarioDTO;
import br.com.dbc.PreSelecao.PreSelecaoApplicationTests;
import br.com.dbc.PreSelecao.entity.Candidato;
import br.com.dbc.PreSelecao.entity.Edicao;
import br.com.dbc.PreSelecao.entity.Endereco;
import br.com.dbc.PreSelecao.entity.Status;
import br.com.dbc.PreSelecao.repository.CandidatoRepository;
import br.com.dbc.PreSelecao.repository.EdicaoRepository;
import br.com.dbc.PreSelecao.repository.EnderecoRepository;
import br.com.dbc.PreSelecao.repository.UserRepository;
import br.com.dbc.PreSelecao.service.CandidatoService;
import br.com.dbc.PreSelecao.service.MailService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author jonas.cruz
 */
public class CandidatoRestControllerTest extends PreSelecaoApplicationTests {

    @Override
    protected AbstractRestController getController() {
        return candidatoRestController;
    }
    @Autowired
    private CandidatoRestController candidatoRestController;

    @Autowired
    private CandidatoService candidatoService;
    
    
    @Autowired
    private EdicaoRepository edicaoRepository;

    @Autowired
    private CandidatoRepository candidatoRepository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private MailService mailService;

    @Before
    public void before() {
        candidatoRepository.deleteAllInBatch();
        edicaoRepository.deleteAllInBatch();
        enderecoRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
    }

    @Test
    public void criarCandidatoTest() throws Exception {
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");     

        Edicao e = Edicao.builder()
                .ano(2019)
                .dataFim(LocalDate.of(2019, Month.MARCH, 20))
                .dataInicio(LocalDate.of(2019, Month.MARCH, 1))
                .dataInicioInscricao(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimInscricao(LocalDate.of(2019, Month.MARCH, 30))
                .dataInicioProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .numeroEdicao(1)
                .build();
        
        Edicao edicao = edicaoRepository.save(e);
        
        
        QuestionarioDTO q = QuestionarioDTO.builder()
                .conhecimentoEmLogica(true)
                .curso("Ciência da Computação")
                .disponibilidadeAposEstagio(true)
                .disponibilidadeDeHorario(true)
                .inspiracao("pudim")
                .instituicao("UNISINOS")
                .matriculadoEmCurso(true)
                .motivo("pudim")
                .referencias("pudim")
                .turnos("Manha, tarde e noite")
                .build();

        CandidatoDTO dto = CandidatoDTO.builder()
                .nomeCompleto("candidato")
                .nascimento(LocalDate.now())
                .cpf("000.000.000-00")
                .telefone("000000000")
                .celular("000000000")
                .cep("91225002")
                .logradouro("rua")
                .numero(Long.MIN_VALUE)
                .bairro("bairoo")
                .cidade("cidade")
                .estado("estado")
                .email("candidato@dbccompany.com.br")
                .curriculo("xxxxxx")
                .questionario(q)
                .build();

        Candidato candidato = objectMapper.readValue(
                restMockMvc.perform(MockMvcRequestBuilders.post("/api/candidato/dto")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsBytes(dto)))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.nomeCompleto").value(dto.getNomeCompleto()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.dataNascimento").value(dto.getNascimento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.cpf").value(dto.getCpf()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(Status.PENDENTE.name()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.endereco.id").isNumber())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.endereco.logradouro").value(dto.getLogradouro()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.endereco.numero").value(dto.getNumero()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.endereco.bairro").value(dto.getBairro()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.endereco.cidade").value(dto.getCidade()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.endereco.estado").value(dto.getEstado()))
                        .andReturn().getResponse().getContentAsString(), Candidato.class);

        List<Candidato> candidatos = candidatoRepository.findAll();

        assertEquals(1, candidatos.size());

        assertEquals(candidato.getId(), candidatos.get(0).getId());
        assertEquals(dto.getNomeCompleto(), candidatos.get(0).getNomeCompleto());
        assertEquals(dto.getNascimento(), candidatos.get(0).getDataNascimento());
        assertEquals(dto.getCpf(), candidatos.get(0).getCpf());
        assertEquals(dto.getTelefone(), candidatos.get(0).getTelefone());

        List<Endereco> enderecos = enderecoRepository.findAll();

        assertEquals(dto.getLogradouro(), candidatos.get(0).getEndereco().getLogradouro());
        assertEquals(dto.getNumero(), candidatos.get(0).getEndereco().getNumero());
        assertEquals(dto.getBairro(), candidatos.get(0).getEndereco().getBairro());
        assertEquals(dto.getCidade(), candidatos.get(0).getEndereco().getCidade());
        assertEquals(dto.getEstado(), candidatos.get(0).getEndereco().getEstado());

        assertEquals(dto.getLogradouro(), enderecos.get(0).getLogradouro());
        assertEquals(dto.getNumero(), enderecos.get(0).getNumero());
        assertEquals(dto.getBairro(), enderecos.get(0).getBairro());
        assertEquals(dto.getCidade(), enderecos.get(0).getCidade());
        assertEquals(dto.getEstado(), enderecos.get(0).getEstado());

    }

    @Test
    public void feedBackCandidatoAprovadoTest() throws Exception {
        Edicao e = Edicao.builder()
                .ano(2019)
                .dataFim(LocalDate.of(2019, Month.MARCH, 20))
                .dataInicio(LocalDate.of(2019, Month.MARCH, 1))
                .dataInicioInscricao(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimInscricao(LocalDate.of(2019, Month.MARCH, 30))
                .dataInicioProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .numeroEdicao(1)
                .build();
        
        Edicao edicao = edicaoRepository.save(e);
        
        QuestionarioDTO q = QuestionarioDTO.builder()
                .conhecimentoEmLogica(true)
                .curso("Ciência da Computação")
                .disponibilidadeAposEstagio(true)
                .disponibilidadeDeHorario(true)
                .inspiracao("pudim")
                .instituicao("UNISINOS")
                .matriculadoEmCurso(true)
                .motivo("pudim")
                .referencias("pudim")
                .turnos("Manha, tarde e noite")
                .build();

        CandidatoDTO dtoParaCriar = CandidatoDTO.builder()
                .nomeCompleto("ocandidato")
                .nascimento(LocalDate.now())
                .cpf("000.000.000-00")
                .telefone("000000000")
                .celular("000000000")
                .cep("91225002")
                .logradouro("rua")
                .numero(Long.MIN_VALUE)
                .bairro("bairoo")
                .cidade("cidade")
                .estado("estado")
                .email("jaqueline.pazbonoto@gmail.com")
                .curriculo("xxxxxx")
                .questionario(q)
                .build();
        Candidato candidato1 = candidatoService.salvaCandidatoComEndereco(dtoParaCriar);
        CandidatoFeedBackDTO dto = CandidatoFeedBackDTO.builder()
                .id(candidato1.getId())
                .status(Status.CONVITE_ENVIADO)
                .build();

        String message = ("<!doctype html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "	<title>Vem Ser DBC</title>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Ol&aacute;,</span></span></p>\n" +
                        "\n" +
                        "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Agradecemos imensamente o teu interesse e inscri&ccedil;&atilde;o no Programa Vem Ser DBC.</span></span></p>\n" +
                        "\n" +
                        "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Gostar&iacute;amos de informar que voc&ecirc; foi selecionado para a pr&oacute;xima etapa. Parab&eacute;ns!</span></span></p>\n" +
                        "\n"
                        + "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Em breve, voc&ecirc; receber&aacute; informa&ccedil;&otilde;es sobre data e hora.</span></span></p>\n" +
                        "\n" +
                        "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><strong><span style=\"color:#C94062;\">Estar pr&oacute;ximo</span><span style=\"color:#3F56A1;\"> &eacute; crescer juntos.</span></strong></span></p>\n" +
                        "\n" +
                        "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Atenciosamente,</span></span></p>\n" +
                        "\n" +
                        "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\">&nbsp;</p>\n" +
                        "\n" +
                        "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"m_-8724565486041811384MsoNormalTable\" style=\"color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: rgb(255, 255, 255); border-collapse: collapse;\">\n" +
                        "	<tbody>\n" +
                        "		<tr style=\"height: 52.1pt;\">\n" +
                        "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 118.1pt; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid rgb(11, 93, 169); padding: 0cm 5.4pt; height: 52.1pt;\" width=\"157\">\n" +
                        "			<p align=\"center\" class=\"MsoNormal\" style=\"margin: 0px; text-align: center;\"><a data-saferedirecturl=\"https://www.google.com/url?q=https://www.facebook.com/VemSerDBC&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNEUA-BWbzqrgStYvzbXtvBYSuA3eA\" href=\"https://www.facebook.com/VemSerDBC\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; color: rgb(31, 73, 125); text-decoration-line: none;\"><img alt=\"cid:image001.jpg@01D42F43.8C41E4F0\" border=\"0\" class=\"CToWUd\" data-image-whitelisted=\"\" height=\"91\" id=\"m_-8724565486041811384Imagem_x0020_18\" src=\"https://uploaddeimagens.com.br/images/001/784/375/original/logoVemSer.jpg?1545047977\" width=\"143\" /></span></a></p>\n" +
                        "			</td>\n" +
                        "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 129pt; padding: 0cm 5.4pt; height: 52.1pt;\" valign=\"top\" width=\"172\">\n" +
                        "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">&nbsp;</span></b></p>\n" +
                        "\n" +
                        "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">Equipe Vem Ser DBC</span></b></p>\n" +
                        "\n" +
                        "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><u><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\"><a data-saferedirecturl=\"https://www.google.com/url?q=http://www.dbccompany.com.br/&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNFW46eVp1pc4aDt8VF1sA3Zmhs9Dw\" href=\"http://www.dbccompany.com.br/\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"color: rgb(63, 86, 161);\">dbc</span><span style=\"color: rgb(201, 64, 98);\">company</span><span style=\"color: rgb(63, 86, 161);\">.com.br</span></a></span></u></b></p>\n" +
                        "			</td>\n" +
                        "		</tr>\n" +
                        "	</tbody>\n" +
                        "</table>\n" +
                        "\n" +
                        "<div>&nbsp;</div>\n" +
                        "</body>\n" +
                        "</html>");

        Mockito.doNothing().when(mailService).sendMail(
                "emailparatestarfuncionalidades@gmail.com",
                dtoParaCriar.getEmail(),
                "Resultado do Processo Seletivo do Programa Vem Ser",
                message
        );

        restMockMvc.perform(MockMvcRequestBuilders.patch("/api/candidato/{id}", candidato1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(Status.CONVITE_ENVIADO.name()));
        List<Candidato> candidatos = candidatoRepository.findAll();
        assertEquals(1, candidatos.size());
        assertEquals(candidato1.getId(), candidatos.get(0).getId());
        assertEquals(dto.getStatus(), candidatos.get(0).getStatus());
        Mockito.verify(mailService, times(1)).sendMail(
                "emailparatestarfuncionalidades@gmail.com",
                dtoParaCriar.getEmail(),
                "Resultado do Processo Seletivo do Programa Vem Ser",
                message
        );
    }

    @Test
    public void feedBackCandidatoReprovadoTest() throws Exception {
        QuestionarioDTO q = QuestionarioDTO.builder()
                .conhecimentoEmLogica(true)
                .curso("Ciência da Computação")
                .disponibilidadeAposEstagio(true)
                .disponibilidadeDeHorario(true)
                .inspiracao("pudim")
                .instituicao("UNISINOS")
                .matriculadoEmCurso(true)
                .motivo("pudim")
                .referencias("pudim")
                .turnos("Manha, tarde e noite")
                .build();
        
        Edicao e = Edicao.builder()
                .ano(2019)
                .dataFim(LocalDate.of(2019, Month.MARCH, 20))
                .dataInicio(LocalDate.of(2019, Month.MARCH, 1))
                .dataInicioInscricao(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimInscricao(LocalDate.of(2019, Month.MARCH, 30))
                .dataInicioProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .numeroEdicao(1)
                .build();
        
        Edicao edicao = edicaoRepository.save(e);

        CandidatoDTO dtoParaCriar = CandidatoDTO.builder()
                .nomeCompleto("candidate")
                .nascimento(LocalDate.now())
                .cpf("000.000.000-99")
                .telefone("000000000")
                .celular("000000000")
                .cep("91225002")
                .logradouro("rua")
                .numero(Long.MIN_VALUE)
                .bairro("bairoo")
                .cidade("cidade")
                .estado("estado")
                .email("jaqueline.pazbonoto@gmail.com")
                .curriculo("xxxxxx")
                .questionario(q)
                .build();
        Candidato candidatte = candidatoService.salvaCandidatoComEndereco(dtoParaCriar);

        CandidatoFeedBackDTO dto = CandidatoFeedBackDTO.builder()
                .id(candidatte.getId())
                .status(Status.REJEITADA)
                .build();
                
        String message = (
                "<!doctype html>\n" +
                "<html>\n" +
                "<head>\n" +
                "	<title>Vem Ser DBC</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Ol&aacute;,</span></span></p>\n" +
                "\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Agradecemos imensamente o teu interesse e inscri&ccedil;&atilde;o no Programa Vem Ser DBC.</span></span></p>\n" +
                "\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Foram mais de 250 inscri&ccedil;&otilde;es e 90 avan&ccedil;aram para a etapa classificat&oacute;ria que definir&aacute; os participantes que ocupar&atilde;o as 14 vagas.</span></span></p>\n" +
                "\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Neste momento n&atilde;o foste selecionado(a) para participar da sele&ccedil;&atilde;o do programa.</span></span></p>\n" +
                "\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Por favor, n&atilde;o deixe de se inscrever nas pr&oacute;ximas edi&ccedil;&otilde;es para participar. Fique ligado nas nossas redes sociais!</span></span></p>\n" +
                "\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><strong><span style=\"color:#C94062;\">Estar pr&oacute;ximo</span><span style=\"color:#3F56A1;\"> &eacute; crescer juntos.</span></strong></span></p>\n" +
                "\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color:#3F56A1;\">Atenciosamente,</span></span></p>\n" +
                "\n" +
                "<p style=\"color: rgb(51, 51, 51); font-size: 13px;\">&nbsp;</p>\n" +
                "\n" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"m_-8724565486041811384MsoNormalTable\" style=\"color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; background-color: rgb(255, 255, 255); border-collapse: collapse;\">\n" +
                "	<tbody>\n" +
                "		<tr style=\"height: 52.1pt;\">\n" +
                "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 118.1pt; border-top: none; border-bottom: none; border-left: none; border-image: initial; border-right: 1pt solid rgb(11, 93, 169); padding: 0cm 5.4pt; height: 52.1pt;\" width=\"157\">\n" +
                "			<p align=\"center\" class=\"MsoNormal\" style=\"margin: 0px; text-align: center;\"><a data-saferedirecturl=\"https://www.google.com/url?q=https://www.facebook.com/VemSerDBC&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNEUA-BWbzqrgStYvzbXtvBYSuA3eA\" href=\"https://www.facebook.com/VemSerDBC\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; color: rgb(31, 73, 125); text-decoration-line: none;\"><img alt=\"cid:image001.jpg@01D42F43.8C41E4F0\" border=\"0\" class=\"CToWUd\" data-image-whitelisted=\"\" height=\"91\" id=\"m_-8724565486041811384Imagem_x0020_18\" src=\"https://uploaddeimagens.com.br/images/001/784/375/original/logoVemSer.jpg?1545047977\" width=\"143\" /></span></a></p>\n" +
                "			</td>\n" +
                "			<td style=\"font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; width: 129pt; padding: 0cm 5.4pt; height: 52.1pt;\" valign=\"top\" width=\"172\">\n" +
                "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">&nbsp;</span></b></p>\n" +
                "\n" +
                "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\">Equipe Vem Ser DBC</span></b></p>\n" +
                "\n" +
                "			<p class=\"MsoNormal\" style=\"margin: 0px;\"><b><u><span style=\"font-size: 13pt; font-family: Calibri, sans-serif; color: rgb(63, 86, 161);\"><a data-saferedirecturl=\"https://www.google.com/url?q=http://www.dbccompany.com.br/&amp;source=gmail&amp;ust=1545131827805000&amp;usg=AFQjCNFW46eVp1pc4aDt8VF1sA3Zmhs9Dw\" href=\"http://www.dbccompany.com.br/\" style=\"color: rgb(17, 85, 204);\" target=\"_blank\"><span style=\"color: rgb(63, 86, 161);\">dbc</span><span style=\"color: rgb(201, 64, 98);\">company</span><span style=\"color: rgb(63, 86, 161);\">.com.br</span></a></span></u></b></p>\n" +
                "			</td>\n" +
                "		</tr>\n" +
                "	</tbody>\n" +
                "</table>\n" +
                "\n" +
                "<div>&nbsp;</div>\n" +
                "</body>\n" +
                "</html>"
        );
        Mockito.doNothing().when(mailService).sendMail(
                "emailparatestarfuncionalidades@gmail.com",
                dtoParaCriar.getEmail(),
                "Resultado do Processo Seletivo do Programa Vem Ser",
                message
        );

        restMockMvc.perform(MockMvcRequestBuilders.patch("/api/candidato/{id}", candidatte.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(Status.REJEITADA.name()));
        List<Candidato> candidatos = candidatoRepository.findAll();
        assertEquals(1, candidatos.size());
        assertEquals(candidatte.getId(), candidatos.get(0).getId());
        assertEquals(dto.getStatus(), candidatos.get(0).getStatus());

        Mockito.verify(mailService, times(1)).sendMail(
                "emailparatestarfuncionalidades@gmail.com",
                dtoParaCriar.getEmail(),
                "Resultado do Processo Seletivo do Programa Vem Ser",
                message
        );
    }

    @Test
    public void retornoConfirmacaoCandidatoTest() throws Exception {
        Edicao e = Edicao.builder()
                .ano(2019)
                .dataFim(LocalDate.of(2019, Month.MARCH, 20))
                .dataInicio(LocalDate.of(2019, Month.MARCH, 1))
                .dataInicioInscricao(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimInscricao(LocalDate.of(2019, Month.MARCH, 30))
                .dataInicioProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .numeroEdicao(1)
                .build();
        
        Edicao edicao = edicaoRepository.save(e);
        
        QuestionarioDTO q = QuestionarioDTO.builder()
                .conhecimentoEmLogica(true)
                .curso("Ciência da Computação")
                .disponibilidadeAposEstagio(true)
                .disponibilidadeDeHorario(true)
                .inspiracao("pudim")
                .instituicao("UNISINOS")
                .matriculadoEmCurso(true)
                .motivo("pudim")
                .referencias("pudim")
                .turnos("Manha, tarde e noite")
                .build();

        CandidatoDTO dtoParaCriar = CandidatoDTO.builder()
                .nomeCompleto("ocandidato")
                .nascimento(LocalDate.now())
                .cpf("00000000000")
                .telefone("000000000")
                .celular("000000000")
                .cep("91225002")
                .logradouro("rua")
                .numero(Long.MIN_VALUE)
                .bairro("bairoo")
                .cidade("cidade")
                .estado("estado")
                .email("jaqueline.pazbonoto@gmail.com")
                .curriculo("xxxxxx")
                .questionario(q)
                .build();
        Candidato candidato1 = candidatoService.salvaCandidatoComEndereco(dtoParaCriar);

        CandidatoFeedBackDTO dto = CandidatoFeedBackDTO.builder()
                .id(candidato1.getId())
                .status(Status.PRESENCA_CONFIRMADA)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.patch("/api/candidato/{id}", candidato1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(Status.PRESENCA_CONFIRMADA.name()));
        List<Candidato> candidatos = candidatoRepository.findAll();
        assertEquals(1, candidatos.size());
        assertEquals(candidato1.getId(), candidatos.get(0).getId());
        assertEquals(dto.getStatus(), candidatos.get(0).getStatus());
    }

    @Test
    public void verificaCPFExistenteTest() throws Exception {
        Edicao e = Edicao.builder()
                .ano(2019)
                .dataFim(LocalDate.of(2019, Month.MARCH, 20))
                .dataInicio(LocalDate.of(2019, Month.MARCH, 1))
                .dataInicioInscricao(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimInscricao(LocalDate.of(2019, Month.MARCH, 30))
                .dataInicioProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimProcessoSeletivo(LocalDate.of(2012, Month.MARCH, 1))
                .numeroEdicao(1)
                .build();
        
        Edicao edicao = edicaoRepository.save(e);
        
        QuestionarioDTO q = QuestionarioDTO.builder()
                .conhecimentoEmLogica(true)
                .curso("Ciência da Computação")
                .disponibilidadeAposEstagio(true)
                .disponibilidadeDeHorario(true)
                .inspiracao("pudim")
                .instituicao("UNISINOS")
                .matriculadoEmCurso(true)
                .motivo("pudim")
                .referencias("pudim")
                .turnos("Manha, tarde e noite")
                .build();

        CandidatoDTO dto = CandidatoDTO.builder()
                .nomeCompleto("aquelecandidato")
                .nascimento(LocalDate.now())
                .cpf("12345678903")
                .telefone("000000000")
                .celular("000000000")
                .cep("91225002")
                .logradouro("rua")
                .numero(Long.MIN_VALUE)
                .bairro("bairoo")
                .cidade("cidade")
                .estado("estado")
                .email("jonas@gmail.com")
                .curriculo("xxxxxx")
                .questionario(q)
                .build();
        
        Candidato candidato2 = null;
        
        try {
            candidato2 = candidatoService.salvaCandidatoComEndereco(dto);
            assertEquals("aquelecandidato", candidato2.getNomeCompleto());
            Candidato retorno = candidatoService.findByCpf(candidato2.getCpf());

            List<Candidato> candidatos = candidatoRepository.findAll();
            assertEquals(1, candidatos.size());
            assertEquals(new Long(1), candidatos.get(0).getId());
            assertEquals(candidato2.getCpf(), retorno.getCpf());
        }
        catch (Exception e2) {
            assertNull(candidato2);
        }

        

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/candidato/cpf/12345678903")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeCompleto").value(dto.getNomeCompleto()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataNascimento").value(dto.getNascimento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cpf").value(dto.getCpf()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(Status.PENDENTE.name()));
    }

    @Test
    public void verificaEmailExistenteTest() throws Exception {
        Edicao e = Edicao.builder()
                .ano(2019)
                .dataFim(LocalDate.of(2019, Month.MARCH, 20))
                .dataInicio(LocalDate.of(2019, Month.MARCH, 1))
                .dataInicioInscricao(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimInscricao(LocalDate.of(2019, Month.MARCH, 30))
                .dataInicioProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .numeroEdicao(1)
                .build();
        
        Edicao edicao = edicaoRepository.save(e);
        
        QuestionarioDTO q = QuestionarioDTO.builder()
                .conhecimentoEmLogica(true)
                .curso("Ciência da Computação")
                .disponibilidadeAposEstagio(true)
                .disponibilidadeDeHorario(true)
                .inspiracao("pudim")
                .instituicao("UNISINOS")
                .matriculadoEmCurso(true)
                .motivo("pudim")
                .referencias("pudim")
                .turnos("Manha, tarde e noite")
                .build();

        CandidatoDTO dto = CandidatoDTO.builder()
                .nomeCompleto("aquelecandidato")
                .nascimento(LocalDate.now())
                .cpf("00000000010")
                .telefone("000000000")
                .celular("000000000")
                .cep("91225002")
                .logradouro("rua")
                .numero(Long.MIN_VALUE)
                .bairro("bairoo")
                .cidade("cidade")
                .estado("estado")
                .email("jonas@gmailcom")
                .curriculo("xxxxxx")
                .questionario(q)
                .build();
        Candidato candidato2 = candidatoService.salvaCandidatoComEndereco(dto);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/candidato/email/{email}", dto.getEmail())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(dto.getEmail()));

        List<Candidato> candidatos = candidatoRepository.findAll();
        assertEquals(1, candidatos.size());
        assertEquals(candidato2.getId(), candidatos.get(0).getId());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void serchBetweenDatasTest() throws Exception {
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        DateTimeFormatter formatadorDate = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        Edicao e = Edicao.builder()
                .ano(2019)
                .dataFim(LocalDate.of(2019, Month.MARCH, 20))
                .dataInicio(LocalDate.of(2019, Month.MARCH, 1))
                .dataInicioInscricao(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimInscricao(LocalDate.of(2019, Month.MARCH, 30))
                .dataInicioProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .dataFimProcessoSeletivo(LocalDate.of(2019, Month.MARCH, 1))
                .numeroEdicao(1)
                .build();
        
        Edicao edicao = edicaoRepository.save(e);

        QuestionarioDTO q = QuestionarioDTO.builder()
                .conhecimentoEmLogica(true)
                .curso("Ciência da Computação")
                .disponibilidadeAposEstagio(true)
                .disponibilidadeDeHorario(true)
                .inspiracao("pudim")
                .instituicao("UNISINOS")
                .matriculadoEmCurso(true)
                .motivo("pudim")
                .referencias("pudim")
                .turnos("Manha, tarde e noite")
                .build();

        CandidatoDTO dto = CandidatoDTO.builder()
                .nomeCompleto("ocandidato")
                .nascimento(LocalDate.now())
                .cpf("000.000.000-00")
                .telefone("000000000")
                .celular("000000000")
                .cep("91225002")
                .logradouro("rua")
                .numero(Long.MIN_VALUE)
                .bairro("bairoo")
                .cidade("cidade")
                .estado("estado")
                .email("jaqueline.pazbonoto@gmail.com")
                .curriculo("xxxxxx")
                .questionario(q)
                .build();
        Candidato candidato = candidatoService.salvaCandidatoComEndereco(dto);

        String url = "/api/candidato/search?page=0&size=10&sort=id,desc"
                + "&start=" + LocalDate.now().minusDays(1).format(formatadorDate).toString()
                + "&end=" + LocalDate.now().plusDays(1).format(formatadorDate).toString();

        restMockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].nomeCompleto").value(dto.getNomeCompleto()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].dataNascimento").value(dto.getNascimento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].cpf").value(dto.getCpf()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].status").value(Status.PENDENTE.name()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].endereco.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].endereco.logradouro").value(dto.getLogradouro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].endereco.numero").value(dto.getNumero()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].endereco.bairro").value(dto.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].endereco.cidade").value(dto.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].endereco.estado").value(dto.getEstado()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].dataInscricao").value(LocalDateTime.now().format(formatador)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].email").value(dto.getEmail()));
    }
}
